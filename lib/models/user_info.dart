import 'package:json_annotation/json_annotation.dart';

part 'user_info.g.dart';

@JsonSerializable()
class UserInfo {
  String? usr;
  String? pwd;
  String? userImage;
  String? userId;
  String? fullName;
  String? mobile;

  UserInfo({
    this.usr,
    this.pwd,
    this.userId,
    this.userImage,
    this.fullName,
    this.mobile,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);
  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}
