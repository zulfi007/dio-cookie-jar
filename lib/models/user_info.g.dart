// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) {
  return UserInfo(
    usr: json['usr'] as String?,
    pwd: json['pwd'] as String?,
    userId: json['userId'] as String?,
    userImage: json['userImage'] as String?,
    fullName: json['fullName'] as String?,
    mobile: json['mobile'] as String?,
  );
}

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
      'usr': instance.usr,
      'pwd': instance.pwd,
      'userImage': instance.userImage,
      'userId': instance.userId,
      'fullName': instance.fullName,
      'mobile': instance.mobile,
    };
