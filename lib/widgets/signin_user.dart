import 'package:dio_networking/models/user_info.dart';
import 'package:dio_networking/services/auth.dart';
import 'package:dio_networking/utils/dio_client.dart';
import 'package:flutter/material.dart';

class SignInUser extends StatefulWidget {
  const SignInUser({Key? key}) : super(key: key);

  @override
  _SignInUserState createState() => _SignInUserState();
}

class _SignInUserState extends State<SignInUser> {
  late final TextEditingController _nameController;
  late final TextEditingController _passwordController;


  bool isCreating = false;

  @override
  void initState() {
    _nameController = TextEditingController();
    _passwordController = TextEditingController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextField(
            controller: _nameController,
            decoration: InputDecoration(hintText: 'Enter name'),
          ),
          TextField(
            controller: _passwordController,
            decoration: InputDecoration(hintText: 'Enter job'),
          ),
          SizedBox(height: 16.0),
          if (isCreating) CircularProgressIndicator() else ElevatedButton(
                  onPressed: () async {
                    setState(() {
                      isCreating = true;
                    });

                    if (_nameController.text != '' &&
                        _passwordController.text != '') {
                      UserInfo userInfo = UserInfo(
                        usr: _nameController.text,
                        pwd: _passwordController.text,
                      );


                      DioClient _client = new DioClient();
                      var apiService = new AuthService(await _client.init());
                      var result = await apiService.login(usr:userInfo.usr,pwd:userInfo.pwd );
                      // print(await CookieJar().loadForRequest(Uri.parse("https://swizpk.frappe.cloud/api/method/login")));
                      if (result != false) {
                        showDialog(
                          context: context,
                          builder: (context) => Dialog(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text('Name: '),
                                    Text('email: '),

                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }
                    }

                    setState(() {
                      isCreating = false;
                    });
                  },
                  child: Text(
                    'Login user',
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
        ],
      ),
    );
  }
}
