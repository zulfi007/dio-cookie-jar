import 'dart:io';


import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:path_provider/path_provider.dart';

class DioClient {

   Future<Dio> init() async {
    Dio _dio = new Dio();
    _dio.options.baseUrl = "https://swizpk.frappe.cloud/api";
    _dio.options.connectTimeout =5000;
    _dio.options.receiveTimeout =3000;

    CookieJar cookieJar=await addCookieJar();

    _dio.interceptors
      ..add(new Logging())
      ..add(CookieManager(cookieJar));

    // print(await cookieJar.loadForRequest(Uri.parse("https://swizpk.frappe.cloud/")));


    return _dio;
  }

  addCookieJar() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath  = appDocDir.path;
    final CookieJar _cookieJar=  new PersistCookieJar(storage: FileStorage(appDocPath));
    return _cookieJar;
  }
}




class Logging extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    print('REQUEST[${options.method}] => PATH: ${options.path}');
    return super.onRequest(options, handler);
  }


  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print(
      'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}',
    );
    return super.onResponse(response, handler);
  }


  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print(
      'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}',
    );
    return super.onError(err, handler);
  }
}