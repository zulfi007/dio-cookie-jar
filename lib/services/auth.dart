

import 'dart:convert';

import 'package:dio/dio.dart';

class AuthService {
  Dio _client;

  AuthService(this._client);

  Future<bool>  login({usr,pwd}) async {
    try{
      final _ = await _client.post(
        '/method/login',
        data: {"usr": usr, "pwd": pwd},
      );
      return true;
    }
    on DioError catch(ex){
      // Assuming there will be an errorMessage property in the JSON object
      String errorMessage = json.decode(ex.response.toString());
      throw new Exception(errorMessage);
    }
  }

}